package ebf;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FileChooser extends JPanel implements ActionListener {
    private static JButton fileButton, folderButton, compressButton, exportFolderButton, overwriteButton;
    public static JTextArea log;
    private static JFileChooser fc;

    public FileChooser() {
        super(new BorderLayout());



        //Create the log first, because the action listeners
        //need to refer to it.
        log = new JTextArea(20,40);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);

        //Create a file chooser
        fc = new JFileChooser();

        fileButton = new JButton("Select File", createImageIcon());
        fileButton.addActionListener(this);
        fileButton.setPreferredSize(new Dimension(110,25));

        folderButton = new JButton("Select Folder", createImageIcon());
        folderButton.addActionListener(this);
        folderButton.setPreferredSize(new Dimension(120,25));

        exportFolderButton = new JButton("Set Output Folder", createImageIcon());
        exportFolderButton.addActionListener(this);
        exportFolderButton.setPreferredSize(new Dimension(150,25));

        overwriteButton = new JButton("Set Overwrite", createImageIcon());
        overwriteButton.addActionListener(this);
        overwriteButton.setPreferredSize(new Dimension(120,25));

        compressButton = new JButton("Compress", createImageIcon());
        compressButton.addActionListener(this);
        compressButton.setPreferredSize(new Dimension(100,25));

        //For layout purposes, put the buttons in a separate panel
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(fileButton);
        buttonPanel.add(folderButton);
        buttonPanel.add(compressButton);
        buttonPanel.add(exportFolderButton);
        buttonPanel.add(overwriteButton);

        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(logScrollPane, BorderLayout.CENTER);
    }

    public void actionPerformed(ActionEvent e) {

        //Handle open button action.
        if (e.getSource() == fileButton) {
            Compressor.texture=null;
            Compressor.inputFolder=null;
            int returnVal = fc.showOpenDialog(FileChooser.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                Compressor.texture=fc.getSelectedFile();
                append("Selected file: " + Compressor.texture.getName());
            } else {
                Compressor.texture=null;
                append("No file was selected");
            }

            //Handle folder selection.
        } else if (e.getSource() == folderButton) {
            Compressor.texture=null;
            Compressor.inputFolder=null;
            int returnVal = fc.showOpenDialog(FileChooser.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                if(fc.getSelectedFile().isDirectory()){
                    Compressor.inputFolder = fc.getSelectedFile();
                } else {
                    Compressor.inputFolder = fc.getSelectedFile().getParentFile();
                }
                append("Selected folder: " + Compressor.inputFolder.getName());
            } else {
                append("No folder was selected");
            }

            //Handle export folder selection.
        } else if (e.getSource() == exportFolderButton) {
            int returnVal = fc.showOpenDialog(FileChooser.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                if(fc.getSelectedFile().isDirectory()){
                    Compressor.export = fc.getSelectedFile();
                } else {
                    Compressor.export = fc.getSelectedFile().getParentFile();
                }
                append("Selected: " + Compressor.export.getName());
            } else {
                Compressor.export=null;
                append("No file was selected");
            }

            //Handle overwrite selection.
        } else if(e.getSource()==overwriteButton){
            Compressor.export=null;
            append("Image Export set to Overwrite.");
            //Handle save button action.
        } else if (e.getSource()==compressButton){
            Compressor.processTextures();
        }
    }

    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon() {
        java.net.URL imgURL = FileChooser.class.getResource("");
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            append("ERROR: program does not have file access!" );
            return null;
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    public static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Java PNG ebf.Compressor");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(new FileChooser());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void append(String val){
        log.append(val+System.lineSeparator());
        log.setCaretPosition(log.getDocument().getLength());
    }
}
