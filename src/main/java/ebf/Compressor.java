package ebf;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


public class Compressor {

    public static void main(String[] input){

        UIManager.put("swing.boldMetal", Boolean.FALSE);
        FileChooser.createAndShowGUI();
        FileChooser.append("Welcome, please choose a file or folder.");
        FileChooser.append("Compression set to overwrite original file, set an Output Folder to change this.");
    }

    public static File texture=null,inputFolder=null,export=null;

    public static final int blankPixel=0x00000000;

    private static byte[] pixels;
    private static BufferedImage input, output;
    private static long compressTotal =0;


    public static void processTextures(){
        if(inputFolder!=null){
            if(inputFolder.listFiles()!=null) {
                compressTotal=0;
                Thread t = new Thread(){
                    @Override
                    public void run(){
                        int p=0;
                        FileChooser.append("progress 0/"+inputFolder.listFiles().length);
                        for (File f : inputFolder.listFiles()) {
                            p++;
                            if(f!=null) {
                                texture = f;
                                createAWT();
                            }
                            FileChooser.append("\nprogress" + p + "/"+inputFolder.listFiles().length+"\n");
                            System.gc();
                        }
                        FileChooser.append("Process complete!");
                        if(compressTotal>1000000){
                            FileChooser.append("total space saved: " + (compressTotal * 0.000001) + " MB)");
                        } else {
                            FileChooser.append("total space saved: " + (compressTotal * 0.001) + " KB)");
                        }
                    }
                };
                t.start();
            } else {
                FileChooser.append("ERROR: No files in selected folder!");
            }
        } else if(texture!=null){
            createAWT();
        } else {
            FileChooser.append("ERROR: No files selected!");
        }
    }

    private static void createAWT(){
        FileChooser.append("Processing: " + Compressor.texture.getName() +" ...");
        input =null;
        try {
            input = ImageIO.read(texture);
        } catch (Exception e){
            FileChooser.append("ERROR: Could not read image input");
            return;
        }

        //repackage the image to ensure a legible format
        try {
            File f=new File(texture.getAbsolutePath() +".temp.png");
            f.createNewFile();

            ImageIO.write(input, "PNG", f);


            input = ImageIO.read(f);

            f.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }

        pixels = ((DataBufferByte) input.getRaster().getDataBuffer()).getData();

        //create a buffered image and push the data to it
        output = new BufferedImage(input.getWidth(), input.getHeight(), BufferedImage.TYPE_INT_ARGB);

        int i,r,g,b,a,x,y;

        for (x = 0; x < input.getWidth(); x++) {
            for (y = 0; y < input.getHeight(); y++) {

                if(((x + (input.getWidth() * y)) * 4)+2<pixels.length) {
                    i = (x + (input.getWidth() * y)) * 4;
                    a = pixels[i] & 0xff;
                    b = pixels[i + 1] & 0xff;
                    g = pixels[i + 2] & 0xff;
                    r = pixels[i + 3] & 0xff;
                    if (a < 1) {
                        output.setRGB(x, y, blankPixel);
                        continue;
                    }

                    output.setRGB(x, y, (a << 24) | (r << 16) | (g << 8) | b);
                }
            }
        }

        try {
            File f;
            if(export ==null) {
                f=new File(texture.getAbsolutePath() +".test");
            } else {
                f=new File(export.getAbsolutePath() + "/"+texture.getName());
            }
            f.createNewFile();

            ImageIO.write(output, "PNG", f);


            if(f.length()<texture.length()){
                FileChooser.append("Successfully compressed!");
                compressTotal+=(texture.length()-f.length());
                FileChooser.append("Image reduced by: "+ (texture.length()-f.length()) +" bytes. (" +
                        ((texture.length()-f.length())*0.001) + " KB)");
                if(export ==null) {
                    f.renameTo(texture);
                }
            } else {
                if(export==null) {
                    FileChooser.append("Compression would not create a smaller Image, skipping.");
                    f.delete();
                } else {
                    FileChooser.append("Compression would not create a smaller Image, copying original to new folder.");
                    Files.copy(texture.toPath(),f.toPath(), REPLACE_EXISTING);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
