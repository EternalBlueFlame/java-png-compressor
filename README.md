# Java PNG Compressor

A program made in java to do lossless compression of png images.

## Getting started

This project requires importing as an intelij gradle project.
after import open the gradle tab in intelij and select rebuild.
to compile the jar file, select the build menu item and select to build artifacts. In theory the command `gradlew build` should also work

## Installation and Usage
This program is standalone and requires no installation, simply extract and run.

The program has a fully graphical interface, run it from java by simply doing
`java -jar /path/to/file.jar`

- Overwrite mode will replace the original file if the compressed variant is smaller in size.
- Setting an output folder will disable Overwrite and instead export the resulting compressed image to the defined folder.
If the compression would not provide benefit, the original image will be copied to the output folder.
- File selection will select a single image for compression.
- Folder selection will allow you to select all files within a folder for compression.
sub-folders are ignored, and support for them is not currently planned. If someone wishes to contribute sub-folder support, be my guest.
- The compress button will start the process.

## Support
Currently the only support is the issues channels or contacting EternalBlueFlame on discord.

## Roadmap
Currently there are no further development plans for the software, add any suggestions as an issue report.

## Contributing
Contributions are welcome.
Contributions do not provide any form of ownership on the project, and are subject to the digression of the maintainers.

## License
This project is licensed under GNU GPL3, for more information check the License file.

## Project status
This project is effectively on pause until issues or additional goals are found.

## Donations
Patreon: https://www.patreon.com/EternalBlueFlame
Bitcoin: 3HNrphHb5bVgQvzL3uodiw13SasqdZzGxh
